package fileSender;

import java.io.IOException;
import java.net.*;
import java.util.HashMap;


/*
 * This class is used to send beacons to be detected by other hosts. This way the current device will be
 * able to receive files from others.
 *
 * It just sends an empty datagram packet, the receivers just extract the source IP, and save it for a limited time.
 *
 * The destination port is constant: 5001, and the destination IP is 255.255.255.255 (broadcast).
 * Broadcasting might be blocked on some network.
 */
public class DiscoveryThread extends Thread {
	
	private final int INTERVAL_TIME = 2000;//10 secondes
	private final DatagramSocket datagramSocket;
	private int myPort;
	private InetAddress myAddress;
	private HashMap<String, InetAddress> hosts;
	private InetAddress broadCastAddress;
	private boolean isUdpClientStarted = false;
	
	public DiscoveryThread(int myPort) throws UnknownHostException, SocketException {
		super();
		this.myPort = myPort;
		this.myAddress = InetAddress.getLocalHost();
		this.broadCastAddress = InetAddress.getByName("255.255.255.255");
		datagramSocket = new DatagramSocket(this.myPort);
		System.out.println("Starting UDP discovery on port: " + this.myPort);
	}
	
	private void sendBeacon () throws IOException {
		byte[] beacon = new byte[1];
		DatagramPacket packet = new DatagramPacket(beacon, beacon.length, this.broadCastAddress, 5557);
		datagramSocket.send(packet);
        System.out.println("SENDING BEACON");
	}
	
	public void startUDPClient () {
		this.isUdpClientStarted = true;
		this.start();
	}
	
	public void pauseUDPClient () {
		this.isUdpClientStarted = false;
	}
	
	public void stopUDPClient () {
		this.isUdpClientStarted = false;
		if (!this.datagramSocket.isClosed()) {
			this.datagramSocket.close();
		}
	}
	
	@Override
	public void run () {
		while (this.isUdpClientStarted) {
			try {
				sendBeacon();
				Thread.sleep(INTERVAL_TIME);
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String args[]){
        
        try{
            DiscoveryThread discoveryThread = new DiscoveryThread(5000);
            discoveryThread.startUDPClient();
        }catch (UnknownHostException | SocketException e){
            e.printStackTrace();
        }
    }
	
}
