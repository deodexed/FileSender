package fileSender;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.net.SocketException;
import java.net.UnknownHostException;

import static fileSender.Utils.*;

public class Launcher extends Application{
    
    private DiscoveryThread discoveryThread = null;
    
    @Override
    public void start(Stage primaryStage) throws Exception{
        
        Parent root = FXMLLoader.load(getClass().getResource("mainLayout.fxml"));
        initSizeValues();
       initDiscoveryThreads();
       registerEvents(primaryStage);
        primaryStage.setTitle("FileSender 2.0");
        Scene rootScene = new Scene(root, adjustedScreenWidth, adjustedScreenHeight);
        
        String css = getClass().getResource("/resources/custom-style.css").toExternalForm();
        rootScene.getStylesheets().add(css);
        
        primaryStage.setScene(rootScene);
        
        primaryStage.show();
    }
    
    
    
    private void initDiscoveryThreads(){
    
        try{
            discoveryThread = new DiscoveryThread(5001);
            
            discoveryThread.startUDPClient();
        }catch (UnknownHostException | SocketException e){
            e.printStackTrace();
        }
    
    }
    
    private void registerEvents(Stage primaryStage){
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>(){
        
            @Override
            public void handle(WindowEvent event){
                if(discoveryThread != null){
           //         discoveryThread.stopUDPClient();
                }
            }
        });
    }
    
    
    private void initSizeValues(){
        
        Rectangle2D screenSize = Screen.getPrimary().getBounds();
        double screenWidth = screenSize.getWidth();
        double screenHeight = screenSize.getHeight();
        
        adjustedScreenWidth = screenWidth - (screenWidth * FACTOR_WIDTH);
        adjustedScreenHeight = screenHeight - (screenHeight * FACTOR_HEIGHT);
        
        adjustedDialogWidth = adjustedScreenWidth - (adjustedScreenWidth * 0.20);
        adjustedDialogHeight = adjustedScreenHeight - (adjustedScreenHeight * 0.20);
    }
    
    @Override
    public void stop() throws Exception{
        
        super.stop();
        this.discoveryThread.stopUDPClient();
    }
    
    public static void main(String[] args){
        
        
        launch(args);
    }
}
