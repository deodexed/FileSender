package fileSender;

import java.io.File;

/**
 * Created by Morgan LEYOUR on 7/3/18
 **/

/*
 * This class represents the items displayed on the GUI which is used to select the files the user wants to send.
 */
public class ListViewItem{
    
    private File file;
    private String fileName;
    
    public ListViewItem(File file){
        this.file = file;
        if(this.file != null){
            this.fileName = this.file.getName();
        }
    }
    
    public File getFile(){
        return file;
    }
    
    public void setFile(File file){
        this.file = file;
    }
    
    public String getFileName(){
        return file.getName();
    }
    
    public void setFileName(String fileName){
        this.fileName = fileName;
    }
    
    @Override
    public String toString(){
        return this.fileName;
    }
}
