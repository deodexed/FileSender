package fileSender;

import javafx.collections.ObservableList;

import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created by Morgan LEYOUR on 7/3/18
 **/
public abstract class Utils{
    
    public static double adjustedScreenWidth;
    public static double adjustedScreenHeight;
    
    public static double adjustedDialogWidth;
    public static double adjustedDialogHeight;
    
    public static final double FACTOR_WIDTH = 0.50;
    public static final double FACTOR_HEIGHT = 0.45;
    
    public static void sendFiles(ObservableList<ListViewItem> filesToSend, int numberOfFiles, int destinationPort, InetAddress destination) throws IOException{
        
        InetSocketAddress inetSocketAddress = new InetSocketAddress(destination, destinationPort);
        Socket socket = new Socket();
        socket.connect(inetSocketAddress);
    
        if(socket.isConnected()){
            DataOutputStream dataOutputStream = null;
            DataInputStream dataInputStream = null;
            try{
                byte arrayOfByte[] = new byte[524288];
                dataOutputStream = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                dataOutputStream.writeInt(numberOfFiles);
                int numberOfByteRead = 0;
        
                for (ListViewItem currentListViewItem : filesToSend) {
                    File file = currentListViewItem.getFile();
                    dataInputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
                    dataOutputStream.writeUTF(file.getName());
                    dataOutputStream.writeLong(file.length());
                    System.out.println("Writing " + file.getName() + " to output");
                    while((numberOfByteRead = dataInputStream.read(arrayOfByte)) > 0){
                        dataOutputStream.write(arrayOfByte, 0, numberOfByteRead);
                    }
                    dataOutputStream.flush();
                    dataInputStream.close();
                    Thread.sleep(1000);
                    arrayOfByte = new byte[524288];
                }
                Thread.sleep(1000);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            } finally {
                if (dataOutputStream != null) {
                    dataOutputStream.close();
                }
                if(dataInputStream != null){
                    dataInputStream.close();
                }
                socket.close();
            }
        }
    }
    

}
