package fileSender;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;


public class Controller{
    
    @FXML
    Label currentHost_label;
    
    @FXML
    ProgressBar progressBar;
    
    @FXML
    Label currentDirectory;
    
    @FXML
    ListView<ListViewItem> mainListView;
    
    @FXML
    Button SEND_button;
    
    @FXML
    Button QUIT_button;
    
    private ObservableList<ListViewItem> listOfItems;
    
    private ObservableList<ListViewItem> currentlySelectedFiles;
    
    private String homedir = System.getProperty("user.home");
    
    private Path currentPath = Paths.get(homedir);
    
    
    public void initialize(){
        
        registerEvents();
        
        listOfItems = FXCollections.observableArrayList();
        
        MultipleSelectionModel<ListViewItem> multipleSelectionModel = mainListView.getSelectionModel();
        multipleSelectionModel.setSelectionMode(SelectionMode.MULTIPLE);
        
        
        getFilesFromDirectory(currentPath);//retrieve all the files from the current directory
        mainListView.setItems(listOfItems);
        progressBar.setVisible(false);
        currentHost_label.setVisible(false);
    }
    
    private void registerEvents(){
        
        QUIT_button.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.NONE, "Are you sure you want to exit ?", ButtonType.YES, ButtonType.NO);
            alert.setTitle("Confirm exit");
            alert.showAndWait();
            
            if (alert.getResult() == ButtonType.YES){
                Platform.exit();
            }
            
        });
        
        SEND_button.setOnAction(event -> {
            if (currentlySelectedFiles.size() > 0){
                
                try{
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("destinationChooserLayout.fxml"));
                    Parent root = null;
                    root = fxmlLoader.load();
                    Stage destinationChooserStage = new Stage();
                    DestinationChooserController destinationChooserController = fxmlLoader.getController();
                    destinationChooserStage.setScene(new Scene(root, 1000, 600));
                    
                    //Launch the hosts selection GUI, stop the main stage
                    destinationChooserStage.showAndWait();
                    
                    //Now we are in the main stage
                    
                    if (destinationChooserController.isOkButtonClicked()){//Check if we pressed the OK button when selecting the hosts, cancel otherwise
                        ObservableList<Host> currentlySelectedHosts = destinationChooserController.getCurrentlySelectedHosts();
                        progressBar.setVisible(true);
                        currentHost_label.setVisible(true);
                        progressBar.setProgress(0.0);
                        if (currentlySelectedHosts != null){
                            Task<String> sendingTask = new Task<String>(){
                                
                                @Override
                                protected String call() throws Exception{
                                    
                                    for (int i = 0; i < currentlySelectedHosts.size(); i++){
                                        Host h = currentlySelectedHosts.get(i);
                                        this.updateMessage("Sending file(s) to host: " + h.toString());
                                        Utils.sendFiles(currentlySelectedFiles, currentlySelectedFiles.size(), 5003, h.getAddress());
                                        this.updateProgress(i, currentlySelectedHosts.size());
                                    }
                                    
                                    return null;
                                }
                            };
                            progressBar.progressProperty().bind(sendingTask.progressProperty());
                            currentHost_label.textProperty().bind(sendingTask.messageProperty());
                            
                            sendingTask.setOnSucceeded(event1 -> {
                                progressBar.progressProperty().unbind();
                                currentHost_label.textProperty().unbind();
                                progressBar.setVisible(false);
                                currentHost_label.setVisible(false);
                            });
                            
                            Thread thread = new Thread(sendingTask);
                            thread.setDaemon(true);
                            thread.start();
                        }
                        
                        
                    }
                }catch (IOException e){
                    e.printStackTrace();
                }
                
            }
            
        });
        
        mainListView.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue){
                /*mainListView.getSelectionModel().select(-1);
                SEND_button.setDisable(true);
                currentlySelectedFiles.clear();*/
            }
        });
        
        mainListView.setOnMouseClicked(event -> {
            if (mainListView.getSelectionModel().getSelectedItem() != null){
                SEND_button.setDisable(false);
            }
            
            if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2){
                if (mainListView.getSelectionModel().getSelectedIndex() == 0){
                    
                    if (currentPath.getParent() != null){//check if we are at the root directory
                        currentPath = currentPath.getParent();
                        getFilesFromDirectory(currentPath);
                    }
                    
                }else{
                    File selectedDirectory = mainListView.getSelectionModel().getSelectedItem().getFile();
                    if (selectedDirectory != null){
                        currentPath = selectedDirectory.toPath();
                        if (selectedDirectory.isDirectory()){
                            getFilesFromDirectory(currentPath);
                        }
                    }
                    
                }
                
            }else{
                currentlySelectedFiles = mainListView.getSelectionModel().getSelectedItems();
            }
            
            
        });
        
        mainListView.setCellFactory(param -> new ListCell<ListViewItem>(){
            
            private ImageView imageView = new ImageView();
            
            @Override
            protected void updateItem(ListViewItem item, boolean empty){
                
                super.updateItem(item, empty);
                if (empty){
                    setText(null);
                    setGraphic(null);
                }else{
                    if (item.getFile() != null){
                        if (item.getFile().isDirectory()){
                            imageView.setImage(new Image("resources/outline_folder_black_18dp.png"));
                            
                        }else{
                            imageView.setImage(new Image("resources/outline_description_black_18dp.png"));
                        }
                        setGraphic(imageView);
                        setText(item.getFileName());
                        //setFont(Font.font(22));
                    }else{
                        setGraphic(null);
                        setText("..");
                        //setFont(Font.font(18));
                    }
                    
                    
                }
            }
        });
        
    }
    
    private void getFilesFromDirectory(Path directory){
        
        listOfItems.clear();
        currentDirectory.setText(directory.toString());
        File currentDirectory = directory.toFile();
        File[] filesInCurrentDirectory = currentDirectory.listFiles();
        
        if (currentPath != currentPath.getRoot()){
            ListViewItem defaultItem = new ListViewItem(null);
            defaultItem.setFileName("..");
            listOfItems.add(defaultItem);
        }
        
        if (filesInCurrentDirectory != null){
            for (File file :
                    Objects.requireNonNull(filesInCurrentDirectory)){
                
                listOfItems.add(new ListViewItem(file));
                
            }
        }
        
        
    }
}
