/*
    Created by Morgan LEYOUR on 7/6/18
*/

package fileSender;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.MouseButton;

import java.net.SocketException;
import java.net.UnknownHostException;


/*
 * This class serves as the controller for the GUI used to select the hosts you will send your files to.
 *
 * It displays a list of hosts which are clickable.
 *
 * Once one or more hosts is selected, the send button becomes available. Clicking it result in the files
 * previously selected to start being sent to the destinations.
 */

public class DestinationChooserController{
    
    // TODO: 7/10/18 Handle errors when sending files, create a progress bar for the files.
    
    @FXML
    public ListView<Host> destinationChooser_listview;
    
    @FXML
    private Button OK_button;
    
    @FXML
    private Button CANCEL_button;
    
    private ObservableList<Host> listOfAddress;
    
    private ReceiveDiscoveryPacketThread receiveDiscoveryPacketThread;
    
    private ObservableList<Host> currentlySelectedHosts;
    
    private boolean isOkButtonClicked = false;
    
    public void initialize(){
        
        listOfAddress = FXCollections.observableArrayList();
        MultipleSelectionModel<Host> multipleSelectionModel = destinationChooser_listview.getSelectionModel();
        multipleSelectionModel.setSelectionMode(SelectionMode.MULTIPLE);
        try{
            receiveDiscoveryPacketThread = new ReceiveDiscoveryPacketThread(5557, this.listOfAddress);
            receiveDiscoveryPacketThread.startUDPServer();
            destinationChooser_listview.setItems(listOfAddress);
            registerEvents();
        }catch (UnknownHostException | SocketException e){
            e.printStackTrace();
        }
        
    }
    
    private void registerEvents(){
        
        OK_button.setOnAction(event -> {
            if (currentlySelectedHosts.size() > 0){
                isOkButtonClicked = true;
                receiveDiscoveryPacketThread.stopUDPServer();
                ((Node) event.getSource()).getScene().getWindow().hide();
            }
        });
        
        CANCEL_button.setOnAction(event -> {
            receiveDiscoveryPacketThread.stopUDPServer();
            ((Node) event.getSource()).getScene().getWindow().hide();
            isOkButtonClicked = false;
        });
        
        destinationChooser_listview.setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.PRIMARY){
                currentlySelectedHosts = destinationChooser_listview.getSelectionModel().getSelectedItems();
            }
        });
    }
    
    public ObservableList<Host> getCurrentlySelectedHosts(){
        
        if (this.currentlySelectedHosts.size() > 0){
            return this.currentlySelectedHosts;
        }else{
            return null;
        }
        
    }
    
    public boolean isOkButtonClicked(){
        return isOkButtonClicked;
    }
    
}
