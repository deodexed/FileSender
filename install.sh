#!/bin/bash
if [ "$EUID" -eq 0 ]
    then
    if [ -e ./FileSender2.0.tar ]
    then
        echo "Extracting application data"
        tar xf FileSender2.0.tar
        echo "creating the filesenderuser";
        useradd filesenderuser
        if [ -d /home/filesenderuser ]
        then
            cp ./FileSender.jar /home/filesenderuser/
            cp ./Receiver.jar /home/filesenderuser/
            cp ./filesender.sh /home/filesenderuser/
            ln -s /home/filesenderuser/filesender.sh /usr/bin/FileSender
            mv filesender.service /etc/systemd/system/
            systemctl daemon-reload
            systemctl enable --now filesender.service

            echo "Type FileSender to start the application. Might need a terminal restart"
        fi
        exit 0
    else
        echo "Error application package not found"
        exit 1

    fi

else
    echo "Error: need to be run as root";
    exit 1
fi
